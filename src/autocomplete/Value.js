import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    background: '#d3e0e5',
    fontSize: '13px',
    padding: '5px 8px',
    marginTop: '8px',
    marginRight: '8px',
    border: '1px solid #8DBAD0',
    display: 'inline-block',
  },
})

const Value = ({ children, onRemove }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Value
