import React from 'react'
import { StyleSheet } from 'elementum'
import Value from './Value'

const styles = StyleSheet.create({
  self: {
    textAlign: 'left',
    width: '100%',
  },
})

const Values = ({ values = [], itemKey, onRemove }) => (
  <div className={styles()}>
    {values.map((value, index) => (
      <Value key={index} onRemove={() => onRemove && onRemove(value)}>
        {value[itemKey]}
      </Value>
    ))}
  </div>
)

export default Values
