import React from 'react'
import { StyleSheet } from 'elementum'
import { Layer } from '../layer'
import Item from './Item'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    background: '#f5f5f5',
    border: '2px solid #9e9e9e',
    borderRadius: '4px',
    boxSizing: 'border-box',
    maxHeight: '320px',
    overflow: 'hidden',
    overflowY: 'auto',
  },
})

const Items = ({ items = [], width, itemKey, onSelect, onClose }) => (
  <Layer classes={{ element: 'tether-element5' }} onOutsideClick={onClose}>
    <div className={styles()} style={{ width }}>
      {items.map((item, index) => (
        <Item
          key={index}
          onSelect={() => onSelect && onSelect(item)}
        >
          {item[itemKey]}
        </Item>
      ))}
    </div>
  </Layer>
)

export default Items
