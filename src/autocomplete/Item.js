import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    transition: 'all 0.3s',
    color: '#546e7a',
    cursor: 'pointer',
    width: '100%',
    padding: '7px',
    fontSize: '14px',
    '&:hover': {
      background: '#fff0ac',
    },
  },
})

const Item = ({ children, onSelect }) => (
  <div
    className={styles()}
    onClick={onSelect}
  >
      {children}
  </div>
)

export default Item
