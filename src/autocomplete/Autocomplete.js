import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import { StyleSheet } from 'elementum'
import { Input } from '../input'
import Items from './Items'
import Values from './Values'

const styles = StyleSheet.create({
  self: {
    width: '100%',
  },
})

class Autocomplete extends Component {
  static defaultProps = {
    itemKey: 'value',
    items: [],
    showValues: true,
    value: [],
  }

  constructor(props, context) {
    super(props, context)

    this.state = {
      value: '',
      width: null,
      items: props.items,
      showItems: false,
    }
  }

  componentDidMount() {
    this.onMount()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.items !== this.props.items) {
      this.setState({ items: nextProps.items })
    }
  }

  onMount() {
    const width = findDOMNode(this).offsetWidth

    this.setState({ width })
  }

  onShowItems = () => {
    setTimeout(() => this.setState({ showItems: true }), 50)
  }

  onCloseItems = () => {
    this.setState({ showItems: false })
  }

  onType = ({ target }) => {
    const { onType } = this.props

    this.setState({ value: target.value })

    if (onType) {
      onType(target.value)
    }
  }

  onSelect = (item) => {
    const { value, onChange } = this.props

    const change = value.includes(item) ? value : value.concat(item)

    if (onChange) {
      onChange(change)
    }

    this.setState({ value: '', showItems: false })
  }

  onRemove = (item) => {
    const { value, onChange } = this.props

    if (value.indexOf(item) !== item) {
      value.splice(value.indexOf(item), 1)

      if (onChange) {
        onChange(value)
      }
    }
  }

  renderInput() {
    const { placeholder } = this.props
    const { value } = this.state

    return (
      <div>
        <Input
          value={value}
          placeholder={placeholder}
          onChange={this.onType}
          onFocus={this.onShowItems}
        />
        {this.renderItems()}
      </div>
    )
  }

  renderItems() {
    const { itemKey } = this.props
    const { width, items, showItems, value } = this.state

    if (!showItems) {
      return null
    }

    if (!(value && value.length > 0)) {
      return null
    }

    if (items.length === 0) {
      return null
    }

    return (
      <Items
        width={width}
        items={items}
        itemKey={itemKey}
        onSelect={this.onSelect}
        onClose={this.onCloseItems}
      />
    )
  }

  renderValues() {
    const { value, itemKey, showValues } = this.props

    if (!showValues) {
      return null
    }

    return (
      <Values
        values={value}
        itemKey={itemKey}
        onRemove={this.onRemove}
      />
    )
  }

  render() {
    return (
      <div className={styles()}>
        {this.renderInput()}
        {this.renderValues()}
      </div>
    )
  }
}

export default Autocomplete
