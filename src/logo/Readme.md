Логотип
=======

### Логотип

```
import React from 'react'
import Logo from './Logo'

const LogoExample = () => (
  <div style={{ margin: '20px' }}>
    <Logo />
  </div>
)

export default LogoExample
```

### Логотип с текстом

```
import React from 'react'
import LogoWithText from './LogoWithText'

const LogoWithTextExample = () => (
  <div style={{ margin: '20px' }}>
    <LogoWithText />
  </div>
)

export default LogoWithTextExample
```
