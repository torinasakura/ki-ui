import React from 'react'
import { StyleSheet } from 'elementum'
import { Column, Layout } from 'flex-layouts'

const styles = StyleSheet.create({
  self: {
    border: '10px',
    boxSizing: 'border-box',
    display: 'block',
    fontFamily: 'Roboto, sans-serif',
    cursor: 'pointer',
    textDecoration: 'none',
    margin: 0,
    padding: '20px 56px 16px 24px',
    outline: 'none',
    fontSize: '16px',
    fontWeight: 'inherit',
    transform: 'translate(0px, 0px)',
    color: 'rgba(0, 0, 0, 0.870588)',
    lineHeight: '3rem',
    position: 'relative',
    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    background: 'none',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0, 0.098392)'
    }
  }
})


const ListItem = ({children}) => (
    <div className={styles}>
      <Column>
        <Layout basis='100px'>
          {Title}
        </Layout>
        <Layout basis='100px'>
          {SubTitle}
        </Layout>
        <Layout basis='50px'>
          {Toolbar}
        </Layout>
      </Column>
    </div>
)

export default ListItem
