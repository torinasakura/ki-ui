import React from 'react'
import { Row, Layout } from 'flex-layouts'

const List = ({ children }) => (
  <Row>
    {items.map((item, index) => (
        <Layout key={index}>
          <ListItem {...item} />
        </Layout>
    ))}
  </Row>
)

export default List
