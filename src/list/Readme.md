Поле ввода
==========

### List

```
import React from 'react'
import { Row, Layout } from 'flex-layouts'

const List = ({ children }) => (
  <Row>
    {items.map((item, index) => (
        <Layout key={index}>
          <ListItem {...item} />
        </Layout>
    ))}
  </Row>
)

export default List

```
### ListItem

```
import React from 'react'
import { Layout } from 'flex-layouts'

const ListItem = ({ children }) => (
  <Layout basis='100px'>
    {children}
  </Layout>
)

export default ListItem
```
